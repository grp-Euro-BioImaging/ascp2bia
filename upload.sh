#! /bin/bash

SCRIPTPATH=$( dirname -- ${BASH_SOURCE[0]}; );

SECRET_DIR=$(cat $SCRIPTPATH/.config | grep 'SECRET_DIR' | cut -f2- -d =);

ascp -P33001 -i $SCRIPTPATH/asperaweb_id_dsa.openssh -d $1 bsaspera_w@hx-fasp-1.ebi.ac.uk:$SECRET_DIR/$2;
