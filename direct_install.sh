#! /bin/bash

SCRIPTPATH=$( dirname -- ${BASH_SOURCE[0]}; );

if command -v conda &> /dev/null;
then 	
	conda env create -f $SCRIPTPATH/ascp_env.yml;
fi;

source activate ascp &> /dev/null;
conda activate ascp &> /dev/null;
