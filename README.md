# Scripts to upload image data to Bioimage Archive via ascp


## Installation 

If you do not have miniconda/anaconda installed, use the following command to clone the repository, download/install miniconda and create a conda environment for the aspera-cli package. In this case, ascp is directly available from the command line without activating the environment.

`cd ~ && git clone https://git.embl.de/oezdemir/ascp2bia.git &&
source ~/ascp2bia/set_secret_directory.sh <your_secret_directory> &&
source ~/ascp2bia/install.sh`

If you already have miniconda/anaconda installed and conda is accessible from the command line, use the following command to clone the repository and then directly create and activate the conda environment for the aspera-cli package.

`cd ~ && git clone https://git.embl.de/oezdemir/ascp2bia.git &&
source ~/ascp2bia/set_secret_directory.sh <your_secret_directory> &&
source ~/ascp2bia/direct_install.sh`

## Upload

Use the following shortcut command for upload:

`source ~/ascp2bia/upload.sh </path/to/local/dir> <./path/to/bia/subdir>`

This command is a shortcut for the following:

`ascp -P33001 -i ~/ascp2bia/asperaweb_id_dsa.openssh -d </path/to/local/dir> bsaspera_w@hx-fasp-1.ebi.ac.uk:<your_secret_directory>/<.path/to/bia/subdir>`
