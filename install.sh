#! /bin/bash

SCRIPTPATH=$( dirname -- ${BASH_SOURCE[0]}; );

source ~/.bashrc
mkdir -p ~/Applications;
cd ~/Applications;

# if miniconda3 is not in the path, add it there:
if ! echo $PATH | tr ":" "\n" | grep "conda" &> /dev/null;
then
	echo PATH="$HOME/miniconda3/bin:$PATH" >> $HOME/.bashrc
fi;

# check if conda Miniconda3 already exists, otherwise download it:
if ! ls | grep Miniconda3 &> /dev/null;
then
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh;
else
	echo "Miniconda3 is already downloaded".
fi;

# grant permission for miniconda installation file and install miniconda:
if ! command -v conda &> /dev/null; 
then
	chmod +x Miniconda3-latest-Linux-x86_64.sh;
	./Miniconda3-latest-Linux-x86_64.sh -b -u;
else
	echo "Miniconda3 is already installed."
fi;

cd ~

# Now create the environment for ascp from the yml file:

source ~/.bashrc
if ! ls ~/miniconda3/envs | grep ascp &> /dev/null;
then 	
	conda env create -f $SCRIPTPATH/ascp_env.yml;
fi;

# Update the alias for ascp
if ! alias | grep "ascp" &> /dev/null;
then
	echo 'alias ascp=$HOME/miniconda3/envs/ascp/bin/ascp' >> ~/.bashrc; ~/.bashrc
fi;

source $HOME/.bashrc;

