#! /bin/bash

SCRIPTPATH=$( dirname -- ${BASH_SOURCE[0]}; );

if cat $SCRIPTPATH/.config | grep "SECRET_DIR" &> /dev/null;
then
	sed -i '/SECRET_DIR/d' $SCRIPTPATH/.config
fi;

source $SCRIPTPATH/.config;

if ! cat $SCRIPTPATH/.config | grep "SECRET_DIR" &> /dev/null;
then
	echo SECRET_DIR=$1 >> $SCRIPTPATH/.config;
fi;

source $SCRIPTPATH/.config;
